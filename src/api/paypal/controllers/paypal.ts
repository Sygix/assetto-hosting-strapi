/**
 * A set of functions called "actions" for `paypal`
 */

import { errors } from '@strapi/utils';

const { ApplicationError } = errors;

export default ({ strapi }) => ({
  webhook: (ctx, next) => {
    try {
      const headers = ctx.request.headers;

      //Check if headers has all paypal headers
      if (
        !headers['paypal-auth-algo']
        || !headers['paypal-cert-url']
        || !headers['paypal-transmission-id']
        || !headers['paypal-transmission-sig']
        || !headers['paypal-transmission-time']
      ) {
        ctx.status = 400;
        ctx.body = 'Invalid headers';
        return;
      }

      const eventType = ctx.request.body.event_type;
      const resource = ctx.request.body.resource;
      
      console.log('Event Type:', eventType); //!Temporary

      if (
        eventType === 'BILLING.SUBSCRIPTION.ACTIVATED'
        || eventType === 'BILLING.SUBSCRIPTION.EXPIRED'
        || eventType === 'BILLING.SUBSCRIPTION.CANCELLED'
        || eventType === 'BILLING.SUBSCRIPTION.SUSPENDED'
        || eventType === 'BILLING.SUBSCRIPTION.PAYMENT.FAILED'
        || eventType === 'BILLING.SUBSCRIPTION.UPDATED'
      ) {
        // Vérifier la signature du webhook PayPal
        strapi.service('api::paypal.paypal').verifyWebhookSignature(headers, ctx.request.body)
          .then((verification_status) => {
            if (verification_status) {
              strapi.service('api::order.order').updateOrCreateBySubscriptionId(resource.id, {
                status: resource.status,
                status_update_time: resource.status_update_time,
                update_time: resource.update_time,
                failed_payments_count: resource.billing_info?.failed_payments_count,
                next_billing_time: resource.billing_info?.next_billing_time,
                auto_renewal: resource.auto_renewal,
              });
            }
          });
      }

      if (eventType === 'PAYMENT.SALE.COMPLETED' && resource.billing_agreement_id) {
        strapi.service('api::paypal.paypal').verifyWebhookSignature(headers, ctx.request.body)
          .then((verification_status) => {
            if (verification_status) {
              const subscriptionId = resource.billing_agreement_id;
              strapi.service('api::paypal.paypal').getSubscriptionDetails(subscriptionId)
                .then((subscriptionDetails) => {
                  strapi.service('api::order.order').updateOrCreateBySubscriptionId(subscriptionId, {
                    status: subscriptionDetails.status,
                    status_update_time: subscriptionDetails.status_update_time,
                    update_time: subscriptionDetails.update_time,
                    failed_payments_count: subscriptionDetails.billing_info?.failed_payments_count,
                    next_billing_time: subscriptionDetails.billing_info?.next_billing_time,
                    auto_renewal: subscriptionDetails.auto_renewal,
                  });
                });
            }
          });
      }
    } catch (error) {
      throw new ApplicationError('Something went wrong while processing webhook event', { error });
    } finally {
      ctx.status = 200;
      ctx.body = "ok";
    }
  }
});
