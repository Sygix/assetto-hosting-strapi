export default {
  routes: [
    {
     method: 'POST',
     path: '/paypal',
     handler: 'paypal.webhook',
    },
  ],
};
