/**
 * paypal service
 */

import { errors } from '@strapi/utils';
import axios from "axios";
import { addSeconds, isAfter, parseISO } from 'date-fns';

const { ApplicationError } = errors;

export default ({strapi}) => ({
    getAccessToken: async (): Promise<string> => {
        const paypalStore = strapi.store({
            environment: strapi.config.environment,
            type: 'plugin',
            name: 'paypal',
        });

        let accessToken: string;
        const now = new Date();
        const authentification = await paypalStore.get({ key: 'authentification' });

        const settingsRes = await strapi.entityService.findMany('api::setting.setting', {
            fields: ['paypal_client_id', 'paypal_secret'],
            limit: 1,
        });

        if (authentification && authentification.accessToken && authentification.expires_in) {
            const expirationDate = parseISO(authentification.expires_in);
            if (isAfter(expirationDate, now)) {
                accessToken = authentification.accessToken;
            }
        }
        
        if (!accessToken) {
            const res = await axios.post(`${process.env.PAYPAL_BASE_URL}/v1/oauth2/token`, {
                grant_type: 'client_credentials',
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                auth: {
                    username: settingsRes?.paypal_client_id,
                    password: settingsRes?.paypal_secret,
                },
            });

            await paypalStore.set({
                key: 'authentification',
                value: {
                    accessToken: res.data.access_token,
                    expires_in: addSeconds(now, Number.parseInt(res.data.expires_in)).toISOString(),
                }
            });
            accessToken = res.data.access_token;
        }
        
        return accessToken;
    },

    verifyWebhookSignature: async (headers: any, body: any): Promise<boolean> => {
        const accessToken = await strapi.service('api::paypal.paypal').getAccessToken();
        const settingsRes = await strapi.entityService.findMany('api::setting.setting', {
            fields: ['paypal_webhook_id'],
            limit: 1,
        });

        try {
            const data = {
                auth_algo: headers['paypal-auth-algo'],
                cert_url: headers['paypal-cert-url'],
                transmission_id: headers['paypal-transmission-id'],
                transmission_sig: headers['paypal-transmission-sig'],
                transmission_time: headers['paypal-transmission-time'],
                webhook_id: settingsRes?.paypal_webhook_id,
                webhook_event: body,
            };

            const res = await axios.post(`${process.env.PAYPAL_BASE_URL}/v1/notifications/verify-webhook-signature`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${accessToken}`,
                },
            });

            if (res.data.verification_status === 'SUCCESS') {
                return true;
            }
        } catch (error) {
            throw new ApplicationError('Something went wrong while verifing webhook signature', { error });
        }
        return false;
    },

    getSubscriptionDetails: async (subscriptionId: string): Promise<any> => {
        const accessToken = await strapi.service('api::paypal.paypal').getAccessToken();
        const res = await axios.get(`${process.env.PAYPAL_BASE_URL}/v1/billing/subscriptions/${subscriptionId}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            },
        });
        return res.data;
    }
});
