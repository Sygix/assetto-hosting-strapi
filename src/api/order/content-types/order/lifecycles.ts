export default {

    afterCreate(event) {
        const { id, subscription_id } = event.result;
        //After order created with paypal client sdk, (order_id, subscription_id) update the order infos from paypal
        strapi.service('api::paypal.paypal').getSubscriptionDetails(subscription_id).then((details) => {
            strapi.entityService.findMany('api::product.product', {
                fields: ['id'],
                filters: {
                    prices: {
                        paypal_plan_id: {
                            $eqi: details.plan_id
                        }
                    }
                },
                limit: 1
            }).then((products) => {
                strapi.entityService.update<any, any>('api::order.order', id, {
                    data: {
                        status: details.status,
                        status_update_time: details.status_update_time,
                        start_time: details.start_time,
                        create_time: details.create_time,
                        update_time: details.update_time,
                        email_address: details.subscriber.email_address,
                        subscriber_name: details.subscriber.name.given_name + ' ' + details.subscriber.name.surname,
                        subscriber_country_code: details.subscriber.shipping_address.address.country_code ?? 'N/A',
                        failed_payments_count: details.billing_info.failed_payments_count,
                        next_billing_time: details.billing_info.next_billing_time,
                        auto_renewal: details.auto_renewal,
                        plan_id: details.plan_id,
                        product: products[0]?.id,
                    }
                });
            });
        });
    },

    async beforeUpdate(event) {
        if (!event.params.data.id && !event.params.where.id) return;
        event.state.previous = await strapi.entityService.findOne('api::order.order', event.params.data.id || event.params.where.id, {
            fields: ['id', 'status', 'plan_id'],
            populate: { referral: true }
        });
    },

    afterUpdate(event) {
        // NOTE : generated_amount on referral is calculated on base product price (no sales, no discounts, have taxes, etc.)
        const { state, params } = event;
        const { plan_id, status } = params.data;
        if (!state.previous) return;

        if (
            (state.previous.status === 'CHECKOUT' || state.previous.status === 'APPROVAL_PENDING' || state.previous.status === 'APPROVED') &&
            status === 'ACTIVE' && state.previous.referral
        ) {
            strapi.entityService.findMany('api::product.product', {
                fields: ['id'],
                filters: {
                    prices: {
                        paypal_plan_id: {
                            $eqi: plan_id
                        }
                    }
                },
                populate: ['prices'],
                limit: 1
            }).then(async (products) => {
                const planPrice = products[0].prices.find(price => price.paypal_plan_id === plan_id);
                const referral: any = await strapi.entityService.findOne('api::referral.referral', state.previous.referral.id, {
                    fields: ['percentage'],
                    populate: { benefits: true }
                });
                const benefitsAmount = planPrice.price * (referral.percentage / 100);
                const newBenefits = referral.benefits.map(benefit => {
                    if(benefit.currency === planPrice.currency) {
                        benefit.amount = parseFloat((benefit.amount + benefitsAmount).toFixed(2));
                    }
                    return benefit;
                });
                // Check if a benefit for this currency already exists
                const benefitExists = newBenefits.some(benefit => benefit.currency === planPrice.currency);

                // If not, create a new benefit
                if (!benefitExists) {
                    newBenefits.push({
                        currency: planPrice.currency,
                        amount: parseFloat(benefitsAmount.toFixed(2))
                    });
                }

                strapi.entityService.update<any, any>('api::referral.referral', state.previous.referral.id, {
                    data: {
                        benefits: newBenefits
                    }
                });
            });
        }
    }

};