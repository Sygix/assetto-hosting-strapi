/**
 * order service
 */

import { factories } from '@strapi/strapi';
import { errors } from '@strapi/utils';

const { ApplicationError } = errors;

type OrderInput = {
    status?: string;
    status_update_time?: string;
    start_time?: string;
    create_time?: string;
    update_time?: string;
    email_address?: string;
    subscriber_name?: string;
    subscriber_country_code?: string;
    failed_payments_count?: string;
    next_billing_time?: string;
    auto_renewal?: boolean;
}

export default factories.createCoreService('api::order.order', ({ strapi }) => ({
    updateOrCreateBySubscriptionId: async (subscription_id: string, input: OrderInput) => {
        const order = await strapi.entityService.findMany('api::order.order', { 
            fields: ['id', 'subscription_id'],
            filters: { subscription_id: subscription_id },
            limit: 1,
        });

        if (!order) {
            //No order found in the database create a new one
            return await strapi.entityService.create<any, any>('api::order.order', {
                data: {...input, subscription_id: subscription_id},
            });
        }

        return await strapi.entityService.update<any, any>('api::order.order', order[0].id, {
            fields: ['id', 'status', 'subscription_id', 'status_update_time', 'start_time', 'create_time',
                'update_time', 'email_address', 'subscriber_name', 'subscriber_country_code', 'failed_payments_count',
                'next_billing_time', 'additional_infos', 'order_id', 'payment_source', 'auto_renewal', 'instance_id',
                'delivery_email_address', 'server_location', 'plan_id'],
            data: input,
        });
    },

    // Refresh active orders if the next billing time is older than the current time
    refreshActiveOrders: async () => {
        let nextPage = 1;
        let pageCount = 1;
        const now = new Date().toISOString();

        while (nextPage <= pageCount) {
            const { results, pagination } = await strapi.entityService.findPage('api::order.order', {
                fields: ['id', 'status', 'subscription_id', 'next_billing_time'],
                filters: {
                    status: 'ACTIVE',
                    next_billing_time: { $lt: now }
                },
                page: nextPage,
            });

            for (const order of results) {
                const { subscription_id } = order;
                const subscriptionDetails = await strapi.service('api::paypal.paypal').getSubscriptionDetails(subscription_id);
                await strapi.service('api::order.order').updateOrCreateBySubscriptionId(subscription_id, { 
                    status: subscriptionDetails.status,
                    status_update_time: subscriptionDetails.status_update_time,
                    update_time: subscriptionDetails.update_time,
                    failed_payments_count: subscriptionDetails.billing_info?.failed_payments_count,
                    next_billing_time: subscriptionDetails.billing_info?.next_billing_time,
                    auto_renewal: subscriptionDetails.auto_renewal,
                });
            }

            nextPage = pagination.page + 1;
            pageCount = pagination.pageCount;
        }
    },
}));
