/**
 * referral service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::referral.referral', ({ strapi }) => ({
    increaseHits: async (referralTag: string) => {
        const referral: any = await strapi.entityService.findMany('api::referral.referral', {
            fields: ['hits'],
            filters: {
                tag: {
                    $eq: referralTag,
                },
            },
            limit: 1,
        });
        
        if (referral.length === 1) {
            return await strapi.entityService.update('api::referral.referral', referral[0].id, {
                data: {
                    hits: referral[0].hits + 1,
                } as unknown,
            });
        }
    }
}));
