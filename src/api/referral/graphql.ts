import { sanitize } from "@strapi/utils";

export const Referral = (strapi) => ({ nexus }) => {
    const { toEntityResponse } = strapi.plugin('graphql').service('format').returnTypes;

    const referralMutationTypes = nexus.extendType({
        type: 'Mutation',
        definition(t) {

            // "increaseHits" mutation definition
            t.field('referralIncreaseHits', {
                // Response type
                type: nexus.nonNull('Int'),

                // Args definition
                args: { tag: nexus.nonNull('String') },

                // Resolver definition
                async resolve(parent, args, context) {
                    const contentType = strapi.contentType("api::referral.referral");
                    const { tag } = args;
                    const entities = await strapi.service('api::referral.referral').increaseHits(tag);
                    const res: any = await sanitize.contentAPI.output(entities, contentType, {
                        auth: context.state.auth,
                    });
                    return res.hits;
                }
            });
        }
    });

    return {
        types: [referralMutationTypes],
        resolversConfig: {
            'Mutation.referralIncreaseHits': {
                auth: {
                    scope: ['api::referral.referral.increaseHits']
                }
            }
        }
    };

};