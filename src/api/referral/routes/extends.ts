export default {
    routes: [
      {
        method: 'POST',
        path: '/referral/increase-hits/:tag',
        handler: 'referral.increaseHits',
      }
    ]
  }