/**
 * referral controller
 */

import { factories } from '@strapi/strapi';
import { sanitize } from '@strapi/utils';

const { contentAPI } = sanitize;

export default factories.createCoreController('api::referral.referral', ({ strapi }) => ({
    async increaseHits(ctx) {
        const { tag } = ctx.params;
        if (!tag) return ctx.badRequest('Missing "tag" param in the request URL');
        const contentType = strapi.contentType('api::referral.referral')
        const response = await strapi.service('api::referral.referral').increaseHits(tag);
        const sanitizedResults = await contentAPI.output(response, contentType, ctx.state.auth) as any;
        return {
            data: {
                id: sanitizedResults.id,
                attributes: {
                    hits: sanitizedResults.hits,
                },
            },
        };
    }
}));
