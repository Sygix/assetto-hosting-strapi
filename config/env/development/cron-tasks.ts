export default {
    /**
     * Simple example.
     * Every 5 minutes.
     */
    orderCheck: {
        task: async ({ strapi }) => {
            try {
                strapi.service('api::order.order').refreshActiveOrders();
            } catch (error) {
                console.log(`task error : ${error}`);
            }
        },
        options: {
            rule: "*/5 * * * *",
        },
    },
  };