export default {
    /**
     * Simple example.
     * Every day at 3 am.
     */
    orderCheck: {
        task: async ({ strapi }) => {
            try {
                strapi.service('api::order.order').refreshActiveOrders();
            } catch (error) {
            console.log(`task error : ${error}`);
            }
        },
        options: {
            rule: "0 3 * * *",
        },
    },
  };